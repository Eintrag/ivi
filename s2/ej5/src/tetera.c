#include <GL/glut.h>
#include <math.h>
struct RotateAxis {
    GLfloat angle;
    GLfloat rotate;
};

struct RotateAxis rotateAxisX = {0.0, 0.0};
struct RotateAxis rotateAxisY = {0.0, 0.0};
struct RotateAxis rotateAxisZ = {0.0, 0.0};

short direction = 1;

void changeRotationAxis(unsigned char key, int x, int y) {
    switch(key) {
    case 'x':
        rotateAxisX.rotate = 1.0;
        break;
    case 'y':
        rotateAxisY.rotate = 1.0;
        break;
    case 'z':
        rotateAxisZ.rotate = 1.0;
        break;
    case 's':
        rotateAxisX.rotate = 0.0;
        rotateAxisY.rotate = 0.0;
        rotateAxisZ.rotate = 0.0;
        break;
    }
}

void changeRotationDirection(int key, int x, int y) {
    switch(key) {
    case GLUT_KEY_LEFT:
        direction = -1;
        break;
    case GLUT_KEY_RIGHT:
        direction = 1;
        break;
    }
}
void increaseAngle() {
    if (rotateAxisX.rotate == 1.0) {
        rotateAxisX.angle = fmod((rotateAxisX.angle + direction), 360.0);
    }
    if (rotateAxisY.rotate == 1.0) {
        rotateAxisY.angle = fmod((rotateAxisY.angle + direction), 360.0);
    }
    if (rotateAxisZ.rotate == 1.0) {
        rotateAxisZ.angle = fmod((rotateAxisZ.angle + direction), 360.0);
    }
    glutPostRedisplay();
}

void render () {
    /* Limpieza de buffers */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    /* Carga de la matriz identidad */
    glLoadIdentity();
    /* Posición de la cámara virtual (position, look, up) */
    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

    /* En color blanco */
    glColor3f( 1.0, 1.0, 1.0 );

    /* Renderiza la tetera */
    glRotatef(rotateAxisX.angle, 1.0, 0.0, 0.0);
    glRotatef(rotateAxisY.angle, 0.0, 1.0, 0.0);
    glRotatef(rotateAxisZ.angle, 0.0, 0.0, 1.0);
    glutWireTeapot(1.5);

    /* Intercambio de buffers... Representation ---> Window */

    glutSwapBuffers();
}

void resize (int w, int h) {
    /* Definición del viewport */
    glViewport(0, 0, w, h);

    /* Cambio a transform. vista */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    /* Actualiza el ratio ancho/alto */
    gluPerspective(50., w/(double)h, 1., 10.);
    /* Vuelta a transform. modelo */

    glMatrixMode(GL_MODELVIEW);
}

int main (int argc, char* argv[]) {
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE );
    glutInitWindowSize(640, 480);
    glutCreateWindow( "Ej5 - IVI - Sesion 2" );
    glEnable (GL_DEPTH_TEST);

    /* Registro de funciones de retrollamada */
    glutDisplayFunc(render);
    glutReshapeFunc(resize);
    glutIdleFunc(increaseAngle);
    /* Bucle de renderizado */
    glutKeyboardFunc(changeRotationAxis);
    glutSpecialFunc(changeRotationDirection);

    glutMainLoop();

    return 0;
}
