// ==== Definicion de estructuras ===================================

struct TObject {
	int id;                      // Identificador del patron
	int visible;                 // Es visible el objeto?
	double width;                // Ancho del patron
	double center[2];            // Centro del patron
	double patt_trans[3][4];     // Matriz asociada al patron
	void (*drawme)(double);       // Puntero a funcion drawme
	double vidaMax, vidaActual, ataque, alcanceAtaque;
};

// ==== Definicion de algoritmos ===================================

double dist(TObject object0, TObject object1) {
	double m[3][4], m2[3][4];
	arUtilMatInv(object0.patt_trans, m);
	arUtilMatMul(m, object1.patt_trans, m2);
	return sqrt(pow(m2[0][3], 2) + pow(m2[1][3], 2) + pow(m2[2][3], 2));
}

void print_error(char *error) {
	printf("%s\n", error);
	exit(0);
}

//devuelve el nuevo valor de la vida
double recibir_ataque(double vidaActual, double ataque){
	if(vidaActual < 0){
		return 0;
	}
	return vidaActual - ataque;
}
