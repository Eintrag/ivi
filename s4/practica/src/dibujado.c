#include <math.h>
#include <GL/glut.h>
#include <AR/gsub.h>
#include <AR/video.h>
#include <AR/param.h>
#include <AR/ar.h>

#include "util.c"

// ==== draw****** (Dibujado especifico de cada objeto) =============

void drawHealthBar(double xFromObject, double yFromObject, double zFromObject,
		double barLength, double vida) {
	//vida debe estar entre 0 y 1
	if (vida < 0) {
		vida = 0;
	} else if (vida > 1) {
		vida = 1;
	}
	double barraVerdeLength = barLength * vida;

	glPushMatrix();
	glDisable(GL_LIGHTING); // Desactiva el cálculo de la iluminación

	glLineWidth(20);
	glTranslatef(xFromObject, yFromObject, zFromObject);

	//Dibuja la barra verde (la vida actual)
	glBegin(GL_LINES);
	glColor3ub(0, 255, 0);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(barraVerdeLength, 0, 0);
	//Dibuja la barra roja (la vida que ha perdido)
	glColor3ub(255, 0, 0);
	glVertex3f(barraVerdeLength, 0.0, 0.0);
	glVertex3f(barraVerdeLength + barLength * (1 - vida), 0, 0);
	glEnd();

	glPopMatrix(); // Dejarlo como estaba
}

void drawteapot(double proporcionVida) {
	GLfloat material[] = { 0.0, 0.0, 1.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, material);
	glTranslatef(0.0, 0.0, 60.0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutSolidTeapot(80.0);

	drawHealthBar(-60.0, 100.0, 0.0, 80.0, proporcionVida);
}

void drawcube(double proporcionVida) {
	GLfloat material[] = { 1.0, 0.0, 0.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, material);
	glTranslatef(0.0, 0.0, 40.0);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	glutSolidCube(80.0);

	drawHealthBar(-60.0, 80.0, 0.0, 80.0, proporcionVida);
}

void drawpotion(double size) {
	glPushMatrix();
	GLfloat material[] = { 1.0, 0.0, 0.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, material);
	glTranslatef(0.0, 0.0, 40.0);
	glutSolidSphere(20, 20, 20);
	glPopMatrix();
}

void drawFightEffect(TObject object0, TObject object1, double attackElevationZ, double avanceAtaque) {
	double m[3][4], m2[3][4];
	arUtilMatInv(object0.patt_trans, m);
	arUtilMatMul(m, object1.patt_trans, m2);

	double gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
	argConvGlpara(object0.patt_trans, gl_para);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd(gl_para);   // Cargamos su matriz de transf.

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glTranslatef(m2[0][3] * avanceAtaque, m2[1][3] * avanceAtaque ,
			m2[2][3] * avanceAtaque + attackElevationZ);

	glutWireSphere(3, 20, 20);
}

void drawpeace(double size) {
	size = 80;

	glPushMatrix();
	glDisable(GL_LIGHTING); // Desactiva el cálculo de la iluminación

	glRotatef(90.0, 1.0, 0.0, 0.0);
	glTranslatef(-40.0, 0.0, 0);
	glColor3ub(255, 255, 255);

	glBegin(GL_LINES);
	glLineWidth(20);
	//horizontales
	glVertex3f(0.0, size / 2, -5.0);
	glVertex3f(size, size / 2, -5.0);
	glVertex3f(0.0, size / 2, 5.0);
	glVertex3f(size, size / 2, 5.0);
	//verticales
	glVertex3f(size / 2, 0.0, -5.0);
	glVertex3f(size / 2, size, -5.0);
	glVertex3f(size / 2, 0.0, 5.0);
	glVertex3f(size / 2, size, 5.0);
	glEnd();
	glPopMatrix();

}
