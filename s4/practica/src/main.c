#include <math.h>
#include <GL/glut.h>
#include <AR/gsub.h>
#include <AR/video.h>
#include <AR/param.h>
#include <AR/ar.h>
#include <stdbool.h>
#include "dibujado.c"
#include <stdio.h>

struct TObject *objects = NULL;
int nobjects = 0;
bool pazEnElCampo;
bool curar;
double saludAcurar = 3;
double tick = 0; // determina las diferentes animaciones de combate
double maxTick = 10;

// ==== addObject (Anade objeto a la lista de objetos) ==============
int addObject(char *p, double w, double c[2], void (*drawme)(double)) {
  int pattid;

  if ((pattid = arLoadPatt(p)) < 0)
    print_error((char *) "Error en carga de patron\n");

  nobjects++;
  objects = (struct TObject *) realloc(objects,
      sizeof(struct TObject) * nobjects);

  objects[nobjects - 1].id = pattid;
  objects[nobjects - 1].width = w;
  objects[nobjects - 1].center[0] = c[0];
  objects[nobjects - 1].center[1] = c[1];
  objects[nobjects - 1].drawme = drawme;
  objects[nobjects - 1].vidaMax = 200;
  objects[nobjects - 1].vidaActual = 200;
  objects[nobjects - 1].ataque = 0;
  objects[nobjects - 1].alcanceAtaque = 0;

  return pattid;
}

// ======== cleanup =================================================
static void cleanup(void) {   // Libera recursos al salir ...
  arVideoCapStop();
  arVideoClose();
  argCleanup();
  free(objects);
  exit(0);
}

// ======== keyboard ================================================
static void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 0x1B:
  case 'Q':
  case 'q':
    cleanup();
    break;
  }
}

// ======== draw ====================================================
void draw(int curar, double tick) {
  double gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
  GLfloat light_position[] = { 100.0, -200.0, 200.0, 0.0 };
  int i;

  argDrawMode3D();              // Cambiamos el contexto a 3D
  argDraw3dCamera(0, 0);        // Y la vista de la camara a 3D
  glClear(GL_DEPTH_BUFFER_BIT); // Limpiamos buffer de profundidad
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  for (i = 0; i < nobjects; i++) {
    if (objects[i].visible) {   // Si el objeto es visible
      argConvGlpara(objects[i].patt_trans, gl_para);
      glMatrixMode(GL_MODELVIEW);
      glLoadMatrixd(gl_para);   // Cargamos su matriz de transf.

      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      glLightfv(GL_LIGHT0, GL_POSITION, light_position);
      objects[i].drawme(objects[i].vidaActual / objects[i].vidaMax); // Llamamos a su función de dibujar
    }
  }
  if (objects[0].visible && objects[1].visible) {
    if(!pazEnElCampo){
      if (objects[0].vidaActual > 0 && dist(objects[0], objects[1]) < objects[0].alcanceAtaque) {
        drawFightEffect(objects[0], objects[1], 0, tick / maxTick);
      }
      if (objects[1].vidaActual > 0 && dist(objects[0], objects[1]) < objects[1].alcanceAtaque) {
        drawFightEffect(objects[1], objects[0], 50, tick / maxTick);
      }
    }
  }

  glDisable(GL_DEPTH_TEST);
}

// ======== init ====================================================
static void init(void) {

  ARParam wparam, cparam;   // Parametros intrinsecos de la camara
  int xsize, ysize;          // Tamano del video de camara (pixels)
  double c[2] = { 0.0, 0.0 };  // Centro de patron (por defecto)
// Abrimos dispositivo de video
  if (arVideoOpen((char *) "-dev=/dev/video0") < 0)
    exit(0);
  if (arVideoInqSize(&xsize, &ysize) < 0)
    exit(0);

// Cargamos los parametros intrinsecos de la camara
  if (arParamLoad("data/camera_para.dat", 1, &wparam) < 0)
    print_error((char *) "Error en carga de parametros de camara\n");

  arParamChangeSize(&wparam, xsize, ysize, &cparam);
  arInitCparam(&cparam);   // Inicializamos la camara con "cparam"

// Inicializamos la lista de objetos
  addObject((char *) "data/simple.patt", 120.0, c, drawteapot);
  addObject((char *) "data/identic.patt", 90.0, c, drawcube);
  addObject((char *) "data/10.patt", 90.0, c, drawpotion);
  addObject((char *) "data/11.patt", 90.0, c, drawpeace);

  objects[0].ataque = 1;
  objects[0].alcanceAtaque = 370;

  objects[1].ataque = 2;
  objects[1].alcanceAtaque = 300;

  argInit(&cparam, 1.0, 0, 0, 0, 0);   // Abrimos la ventana
}

// ======== mainLoop ================================================
static void mainLoop(void) {

  tick += 1;
  if (tick > maxTick) {
    tick = 0;
  }
  ARUint8 *dataPtr;
  ARMarkerInfo *marker_info;
  int marker_num, i, j, k;

// Capturamos un frame de la camara de video
  if ((dataPtr = (ARUint8 *) arVideoGetImage()) == NULL) {
    // Si devuelve NULL es porque no hay un nuevo frame listo
    arUtilSleep(2);
    return;  // Dormimos el hilo 2ms y salimos
  }

  argDrawMode2D();
  argDispImage(dataPtr, 0, 0);    // Dibujamos lo que ve la camara

// Detectamos la marca en el frame capturado (return -1 si error)
  if (arDetectMarker(dataPtr, 100, &marker_info, &marker_num) < 0) {
    cleanup();
    exit(0);   // Si devolvio -1, salimos del programa!
  }

  arVideoCapNext();      // Frame pintado y analizado... A por otro!

// Vemos donde detecta el patron con mayor fiabilidad
  for (i = 0; i < nobjects; i++) {
    for (j = 0, k = -1; j < marker_num; j++) {
      if (objects[i].id == marker_info[j].id) {
        if (k == -1)
          k = j;
        else if (marker_info[k].cf < marker_info[j].cf)
          k = j;
      }
    }
    if (k != -1) {   // Si ha detectado el patron en algun sitio...
      objects[i].visible = 1;
      arGetTransMat(&marker_info[k], objects[i].center, objects[i].width,
          objects[i].patt_trans);
    } else {
      objects[i].visible = 0;
    }
  }
  curar = objects[2].visible;
  pazEnElCampo = objects[3].visible;

  if (nobjects > 1) {
    if (objects[0].visible && objects[1].visible) {
      if (curar) {
        if (objects[0].vidaActual < objects[0].vidaMax) {
          objects[0].vidaActual = objects[0].vidaActual + saludAcurar;
        }
        if (objects[1].vidaActual < objects[1].vidaMax) {
          objects[1].vidaActual = objects[1].vidaActual + saludAcurar;
        }
      }

      if (!pazEnElCampo) {
        // Ataca el objeto 0
        if (objects[0].vidaActual > 0 && dist(objects[0], objects[1]) < objects[0].alcanceAtaque) {
          objects[1].vidaActual = recibir_ataque(objects[1].vidaActual,
              objects[0].ataque);
        }
        // Ataca el objeto 1
        if (objects[1].vidaActual > 0 && dist(objects[0], objects[1]) < objects[1].alcanceAtaque) {
          objects[0].vidaActual = recibir_ataque(objects[0].vidaActual,
              objects[1].ataque);
        }
      }
    }
    draw(curar, tick);           // Dibujamos los objetos de la escena
    argSwapBuffers(); // Cambiamos el buffer con lo que tenga dibujado
  }
}

// ======== Main ====================================================
int main(int argc, char **argv) {
  glutInit(&argc, argv);    // Creamos la ventana OpenGL con Glut

  init();                   // Llamada a nuestra funcion de inicio

  arVideoCapStart();        // Creamos un hilo para captura de video
  argMainLoop( NULL, keyboard, mainLoop);    // Asociamos callbacks
  return (0);
}
